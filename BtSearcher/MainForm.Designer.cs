﻿namespace BtSearcher
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txTabControl1 = new TX.Framework.WindowUI.Controls.TXTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txListView1 = new TX.Framework.WindowUI.Controls.TXListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txStatusStrip1 = new TX.Framework.WindowUI.Controls.TXStatusStrip();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txComboBox1 = new TX.Framework.WindowUI.Controls.TXComboBox();
            this.txButton1 = new TX.Framework.WindowUI.Controls.TXButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txTextBox1 = new TX.Framework.WindowUI.Controls.TXTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.txTabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = global::BtSearcher.Properties.Resources.bittorrent_app__1_;
            this.pictureBox1.Location = new System.Drawing.Point(3, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(726, 113);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // txTabControl1
            // 
            this.txTabControl1.BaseTabColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(247)))), ((int)(((byte)(250)))));
            this.txTabControl1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(168)))), ((int)(((byte)(192)))));
            this.txTabControl1.CaptionFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txTabControl1.CheckedTabColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(165)))), ((int)(((byte)(220)))));
            this.txTabControl1.Controls.Add(this.tabPage1);
            this.txTabControl1.Controls.Add(this.tabPage2);
            this.txTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txTabControl1.HeightLightTabColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(67)))), ((int)(((byte)(165)))), ((int)(((byte)(220)))));
            this.txTabControl1.Location = new System.Drawing.Point(3, 140);
            this.txTabControl1.Name = "txTabControl1";
            this.txTabControl1.SelectedIndex = 0;
            this.txTabControl1.Size = new System.Drawing.Size(726, 342);
            this.txTabControl1.TabCornerRadius = 3;
            this.txTabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txListView1);
            this.tabPage1.Controls.Add(this.txStatusStrip1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(718, 309);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "搜索结果";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txListView1
            // 
            this.txListView1.BackColor = System.Drawing.Color.White;
            this.txListView1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(168)))), ((int)(((byte)(192)))));
            this.txListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.txListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txListView1.Font = new System.Drawing.Font("宋体", 9.6F);
            this.txListView1.FullRowSelect = true;
            this.txListView1.HeaderBeginColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(253)))));
            this.txListView1.HeaderEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.txListView1.Location = new System.Drawing.Point(3, 3);
            this.txListView1.Name = "txListView1";
            this.txListView1.OwnerDraw = true;
            this.txListView1.RowBackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(254)))));
            this.txListView1.RowBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(246)))), ((int)(((byte)(253)))));
            this.txListView1.SelectedBeginColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.txListView1.SelectedEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(225)))), ((int)(((byte)(253)))));
            this.txListView1.Size = new System.Drawing.Size(712, 281);
            this.txListView1.TabIndex = 2;
            this.txListView1.UseCompatibleStateImageBehavior = false;
            this.txListView1.View = System.Windows.Forms.View.Details;
            this.txListView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txListView1_MouseClick);
            this.txListView1.MouseEnter += new System.EventHandler(this.txListView1_MouseEnter);
            this.txListView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txListView1_MouseMove);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "序号";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "标题";
            this.columnHeader2.Width = 200;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "详细内容";
            this.columnHeader3.Width = 120;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "说明";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "BT链接";
            this.columnHeader5.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "磁力链接";
            this.columnHeader6.Width = 100;
            // 
            // txStatusStrip1
            // 
            this.txStatusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(247)))), ((int)(((byte)(252)))));
            this.txStatusStrip1.BeginBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(247)))), ((int)(((byte)(252)))));
            this.txStatusStrip1.EndBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(247)))), ((int)(((byte)(252)))));
            this.txStatusStrip1.Location = new System.Drawing.Point(3, 284);
            this.txStatusStrip1.Name = "txStatusStrip1";
            this.txStatusStrip1.Size = new System.Drawing.Size(712, 22);
            this.txStatusStrip1.TabIndex = 1;
            this.txStatusStrip1.Text = "txStatusStrip1";
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(718, 309);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "关于本程序";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txComboBox1
            // 
            this.txComboBox1.FormattingEnabled = true;
            this.txComboBox1.ItemHeight = 12;
            this.txComboBox1.Location = new System.Drawing.Point(10, 100);
            this.txComboBox1.Name = "txComboBox1";
            this.txComboBox1.Size = new System.Drawing.Size(367, 20);
            this.txComboBox1.TabIndex = 2;
            this.txComboBox1.TextChanged += new System.EventHandler(this.txComboBox1_TextChanged);
            // 
            // txButton1
            // 
            this.txButton1.Image = null;
            this.txButton1.Location = new System.Drawing.Point(383, 95);
            this.txButton1.Name = "txButton1";
            this.txButton1.Size = new System.Drawing.Size(100, 28);
            this.txButton1.TabIndex = 3;
            this.txButton1.Text = "搜索BT种子";
            this.txButton1.UseVisualStyleBackColor = true;
            this.txButton1.Click += new System.EventHandler(this.txButton1_Click);
            // 
            // txTextBox1
            // 
            this.txTextBox1.BackColor = System.Drawing.Color.Transparent;
            this.txTextBox1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(168)))), ((int)(((byte)(192)))));
            this.txTextBox1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txTextBox1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txTextBox1.HeightLightBolorColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(67)))), ((int)(((byte)(165)))), ((int)(((byte)(220)))));
            this.txTextBox1.Image = null;
            this.txTextBox1.ImageSize = new System.Drawing.Size(0, 0);
            this.txTextBox1.Location = new System.Drawing.Point(10, 98);
            this.txTextBox1.Name = "txTextBox1";
            this.txTextBox1.Padding = new System.Windows.Forms.Padding(2);
            this.txTextBox1.PasswordChar = '\0';
            this.txTextBox1.Required = false;
            this.txTextBox1.Size = new System.Drawing.Size(367, 22);
            this.txTextBox1.TabIndex = 4;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CapitionLogo = global::BtSearcher.Properties.Resources.bittorrent_app;
            this.ClientSize = new System.Drawing.Size(732, 485);
            this.Controls.Add(this.txTextBox1);
            this.Controls.Add(this.txButton1);
            this.Controls.Add(this.txComboBox1);
            this.Controls.Add(this.txTabControl1);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BTSearcher - BT资源搜索者";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.txTabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private TX.Framework.WindowUI.Controls.TXTabControl txTabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private TX.Framework.WindowUI.Controls.TXListView txListView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private TX.Framework.WindowUI.Controls.TXStatusStrip txStatusStrip1;
        private TX.Framework.WindowUI.Controls.TXComboBox txComboBox1;
        private TX.Framework.WindowUI.Controls.TXButton txButton1;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ToolTip toolTip1;
        private TX.Framework.WindowUI.Controls.TXTextBox txTextBox1;
    }
}

